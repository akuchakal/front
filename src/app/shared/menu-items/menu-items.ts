import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: 'home',
    name: 'HOME',
    type: 'link',
    icon: 'explore'
  },
  {
    state: 'forms',
    name: 'Formularios',
    type: 'sub',
    icon: 'local_library',
    children: [
      {state: 'students', name: 'Persona'},
    ]
  },
  {
    state: 'tables',
    name: 'Tablas',
    type: 'sub',
    icon: 'format_line_spacing',
    children: [
      {state: 'persons', name: 'Personas'},
    ]
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
