import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-form-students',
  templateUrl: './form-students.component.html',
  styleUrls: ['./form-students.component.scss']
})
export class FormStudentsComponent implements OnInit {

    drinks = [{
          value: 'coke-0',
          viewValue: 'Coke'
        }, {
          value: 'sprite-1',
          viewValue: 'Sprite'
        }, {
          value: 'water-2',
          viewValue: 'Water'
        }, {
          value: 'pepper-3',
          viewValue: 'Dr. Pepper'
        }, {
          value: 'coffee-4',
          viewValue: 'Coffee'
        }, {
          value: 'tea-5',
          viewValue: 'Tea'
        }, {
          value: 'juice-6',
          viewValue: 'Orange juice'
        }, {
          value: 'wine-7',
          viewValue: 'Wine'
        }, {
          value: 'milk-8',
          viewValue: 'Milk'
    }];

  constructor() { }

  ngOnInit() {
  }

}
